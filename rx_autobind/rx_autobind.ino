#include <Servo.h>
#include <EEPROM.h>
#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

//#define JEEP4WD
#define LAMBO
//#define FERRARI
//#define JEEP

#ifdef JEEP
#define MOTOR_VIA_ESC
#define MAX_ESC_SIGNAL 2300
#define MIN_ESC_SIGNAL 800
#define STOP_ESC_SIGNAL 1500
#define INVERT_STEERING
#define ESC_PIN 5
#define SERVO_PIN 3
#define BEEP_PIN 2
#define RX_CHANNEL_NO 0x8
#define RX_ID 12
#define INITIAL_SERVO_ANGLE  68.0

#endif

#ifdef FERRARI
#define MOTOR_VIA_HBRIDGE
#define SERVO_PIN 3
#define PWM_FORWARD_PIN 5
#define PWM_BACKWARD_PIN 6
#define TURBO_FORWARD_PIN 4
//#define TURBO_BACKWARD_PIN 7
#define RX_CHANNEL_NO 0x12
#define RX_ID 15
#define INITIAL_SERVO_ANGLE 70.0
#define PWMFREQ1 
#endif

#ifdef LAMBO
#define MOTOR_VIA_HBRIDGE
#define SERVO_PIN 3
#define PWM_FORWARD_PIN 6
#define PWM_BACKWARD_PIN 5
#define ADD_FORWARD_PIN 8
#define ADD_BACKWARD_PIN 7
#define LIGHT_FORWARD_PIN 2
#define LIGHT_BACKWARD_PIN 4

#define INVERT_STEERING
#define RX_CHANNEL_NO 0x60
#define RX_ID 16
#define INITIAL_SERVO_ANGLE 70.0
#endif


#ifdef JEEP4WD
#define MOTOR_VIA_HBRIDGE
#define SERVO_PIN 3
#define PWM_FORWARD_PIN 5
#define PWM_BACKWARD_PIN 6
#define ADD_FORWARD_PIN 7
#define ADD_BACKWARD_PIN 8
#define BEEP_PIN 2
#define INVERT_STEERING
#define RX_CHANNEL_NO 0x65
#define RX_ID 17
#define INITIAL_SERVO_ANGLE 70.0
#endif


RF24 radio(9,10); // "создать" модуль на пинах 9 и 10 Для Уно, нано

#ifdef MOTOR_VIA_ESC
Servo motor;
int motorPin = ESC_PIN;
#endif

byte address[][6] = {"1Node","2Node","3Node","4Node","5Node","6Node"};  //возможные номера труб

const byte BIND_CHANNEL_NO = 0x5;
const byte RECEIVER_CHANNEL_NO = RX_CHANNEL_NO;
const byte RECEIVER_ID = RX_ID;

typedef struct BindData
{
  byte receiverId;
  byte channelNo;
  byte checkSum;
};

BindData bindData;

typedef struct PackageData
{
  unsigned int Throttle : 10;
  unsigned int Steering : 10;
  byte CommandId : 5;
  byte CommandParameter : 7;  
};

PackageData package;

const byte COMMAND_NULL = 0;
const byte COMMAND_NORM_STEERING = 1;
const byte COMMAND_BEEP = 2;
const byte COMMAND_STEERING_RANGE = 3;
const byte COMMAND_STEERING_RANGE_LEFT = 4;
const byte COMMAND_STEERING_RANGE_RIGHT = 5;
const byte COMMAND_STEERING_RANGE_SAVE = 6;


unsigned int mode;
const unsigned int MODE_REGULAR = 0;
const unsigned int MODE_SEARCH_FOR_TRANSMITTER = 1;

#ifdef SERVO_PIN
Servo servo;
int servoPin = SERVO_PIN;
bool servoInit = false;
#endif

#ifdef PWM_FORWARD_PIN
int motorAForward = PWM_FORWARD_PIN;
#endif
#ifdef PWM_BACKWARD_PIN
int motorABackward = PWM_BACKWARD_PIN;
#endif
#ifdef TURBO_FORWARD_PIN
int motorATurbo = TURBO_FORWARD_PIN;
#endif
#ifdef TURBO_BACKWARD_PIN
int motorATurboBackward = TURBO_BACKWARD_PIN;
#endif


/*jeep steering servo angles
float left = 84.0;
float right = 48.0;
float center = 68.0;
*/

/*ferrari steering servo angles
float left = 36.0;
float right = 100.0;
float center = 70.0;
*/

const byte DATA_VERSION_KEY_VALUE = 0x12;

typedef struct SettingsData
{
  byte DataVersionKey;
  float AppLeft;
  float AppCenter;
  float AppRight;
  float SteeringMin;
  float SteeringCenter;
  float SteeringMax;
};

SettingsData settings; 

void loadSettings(SettingsData * data)
{
  int address=0;
  byte * dataPtr = (byte*)(data);
  for(int i=0; i<sizeof(*data); i++)
  {
    *dataPtr = EEPROM.read(address + i);
    dataPtr++;
  }  
}

void saveSettings(SettingsData * data)
{
  int address=0;
  byte * dataPtr = (byte*)(data);
  for(int i=0; i<sizeof(*data); i++)
  {
    EEPROM.update(address + i, *dataPtr);
    dataPtr++;
  }  
}

void setup(){
  mode = MODE_SEARCH_FOR_TRANSMITTER;

#ifdef PWMFREQ1
//---------------------------------------------- Set PWM frequency for D5 & D6 -------------------------------
 
//TCCR0B = TCCR0B & B11111000 | B00000001;    // set timer 0 divisor to     1 for PWM frequency of 62500.00 Hz
//TCCR0B = TCCR0B & B11111000 | B00000010;    // set timer 0 divisor to     8 for PWM frequency of  7812.50 Hz
//  TCCR0B = TCCR0B & B11111000 | B00000011;    // set timer 0 divisor to    64 for PWM frequency of   976.56 Hz (The DEFAULT)
//TCCR0B = TCCR0B & B11111000 | B00000100;    // set timer 0 divisor to   256 for PWM frequency of   244.14 Hz

TCCR0B = TCCR0B & B11111000 | B00000101;    // set timer 0 divisor to  1024 for PWM frequency of    61.04 Hz

//---------------------------------------------- Set PWM frequency for D3 & D11 ------------------------------
 
//TCCR2B = TCCR2B & B11111000 | B00000001;    // set timer 2 divisor to     1 for PWM frequency of 31372.55 Hz
//TCCR2B = TCCR2B & B11111000 | B00000010;    // set timer 2 divisor to     8 for PWM frequency of  3921.16 Hz
//TCCR2B = TCCR2B & B11111000 | B00000011;    // set timer 2 divisor to    32 for PWM frequency of   980.39 Hz
//  TCCR2B = TCCR2B & B11111000 | B00000100;    // set timer 2 divisor to    64 for PWM frequency of   490.20 Hz (The DEFAULT)
//TCCR2B = TCCR2B & B11111000 | B00000101;    // set timer 2 divisor to   128 for PWM frequency of   245.10 Hz
//TCCR2B = TCCR2B & B11111000 | B00000110;    // set timer 2 divisor to   256 for PWM frequency of   122.55 Hz
//TCCR2B = TCCR2B & B11111000 | B00000111;    // set timer 2 divisor to  1024 for PWM frequency of    30.64 Hz
#endif


#ifdef SERVO_PIN  
  pinMode(servoPin, OUTPUT);
  servo.attach(servoPin);
#endif

#ifdef MOTOR_VIA_ESC 
  pinMode(motorPin, OUTPUT);
  motor.attach(motorPin);
  calibrateEsc(&motor);
#endif

#ifdef PWM_FORWARD_PIN
  pinMode(motorAForward, OUTPUT);
  digitalWrite(motorAForward, LOW);
#endif
#ifdef PWM_BACKWARD_PIN
  pinMode(motorABackward, OUTPUT);
  digitalWrite(motorABackward, LOW);  
#endif
#ifdef TURBO_FORWARD_PIN
  pinMode(motorATurbo, OUTPUT);
  digitalWrite(motorATurbo, LOW);  
#endif
#ifdef TURBO_BACKWARD_PIN
  pinMode(motorATurboBackward, OUTPUT);
  digitalWrite(motorATurboBackward, LOW);
#endif
#ifdef ADD_FORWARD_PIN
  pinMode(ADD_FORWARD_PIN, OUTPUT);
  digitalWrite(ADD_FORWARD_PIN, LOW);
#endif
#ifdef ADD_BACKWARD_PIN
  pinMode(ADD_BACKWARD_PIN, OUTPUT);
  digitalWrite(ADD_BACKWARD_PIN, LOW);  
#endif
#ifdef LIGHT_FORWARD_PIN
  pinMode(LIGHT_FORWARD_PIN, OUTPUT);
  digitalWrite(LIGHT_FORWARD_PIN, LOW);
#endif
#ifdef LIGHT_BACKWARD_PIN
  pinMode(LIGHT_BACKWARD_PIN, OUTPUT);
  digitalWrite(LIGHT_BACKWARD_PIN, LOW);  
#endif

#ifdef BEEP_PIN
  pinMode(BEEP_PIN, OUTPUT); //BEEPER
#endif

  bindData.receiverId = RECEIVER_ID;
  bindData.channelNo = RECEIVER_CHANNEL_NO;
  bindData.checkSum = bindData.receiverId + bindData.channelNo;

  Serial.begin(9600); //открываем порт для связи с ПК
  radio.begin(); //активировать модуль

  radio.setAutoAck(true); //режим подтверждения приёма, 1 вкл 0 выкл
  radio.enableAckPayload();    //разрешить отсылку данных в ответ на входящий сигнал

          
  radio.setRetries(10, 1);     //(время между попыткой достучаться, число попыток)
  radio.setPayloadSize(4);     //размер пакета, в байтах

  radio.openWritingPipe(address[0]);   //мы - труба 1, открываем канал для передачи данных для поиска передатчика

  radio.setChannel(BIND_CHANNEL_NO);  //выбираем канал (в котором нет шумов!)

  radio.setPALevel (RF24_PA_MAX); //уровень мощности передатчика. На выбор RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX
  radio.setDataRate (RF24_250KBPS); //скорость обмена. На выбор RF24_2MBPS, RF24_1MBPS, RF24_250KBPS, должна быть одинакова на приёмнике и передатчике! при самой низкой скорости имеем самую высокую чувствительность и дальность!!
  
  radio.powerUp(); //начать работу
  radio.stopListening(); 
  

  loadSettings(&settings);
  if(settings.DataVersionKey != DATA_VERSION_KEY_VALUE)
  {
    settings.DataVersionKey = DATA_VERSION_KEY_VALUE;
    settings.AppLeft = 0.0;
    settings.AppCenter = 511.5;
    settings.AppRight = 1023.0;
    settings.SteeringMin = 0.0;
    settings.SteeringCenter = INITIAL_SERVO_ANGLE;
    settings.SteeringMax = 180.0;
  }
}


void loopRegular()
{
    #ifdef PWMFREQ1
    long timeoutMs = 50;
    #else
    long timeoutMs = 500;
    #endif

  
  byte pipeNo;                          

    unsigned long started_waiting_at = millis();
    bool timeout = false;
    while ( ! radio.available(&pipeNo) && ! timeout )
      if (millis() - started_waiting_at > timeoutMs )
        timeout = true;

    if (timeout)
    {
      Serial.println("Failsafe"); 
#ifdef ESC_PIN
      motor.writeMicroseconds(STOP_ESC_SIGNAL);
#endif;
#ifdef SERVO_PIN
      servo.write(settings.SteeringCenter);
#endif
#ifdef MOTOR_VIA_HBRIDGE
      stopMotorViaHBridge();
#endif
    } 
    else
    { 
      radio.read( &package, sizeof(package) );
      
      int throttle = package.Throttle;
     
#ifdef MOTOR_VIA_ESC
      int val = map(throttle, 0, 1023, MIN_ESC_SIGNAL, MAX_ESC_SIGNAL);
      motor.writeMicroseconds(val);
#endif;

#ifdef MOTOR_VIA_HBRIDGE
      processMotorViaHBridge(throttle);
#endif;

      
      int steering = package.Steering;

      bool invertSteering = 
#ifdef INVERT_STEERING
        true
#else
        false
#endif 
      ;
        
      int servoAngle = calculateServoAngle(steering, invertSteering);
#ifdef SERVO_PIN
      servo.write(servoAngle);
#endif
      Serial.print("Received: A0:"); Serial.print(package.Throttle); Serial.print(" A1:"); Serial.print(package.Steering);Serial.print(" St:");Serial.println(servoAngle);

      switch (package.CommandId) 
      {
        case COMMAND_NORM_STEERING:
        {
          settings.SteeringCenter = servoAngle;
          saveSettings(&settings);
          Serial.print("Center steering: "); Serial.println(servoAngle);  
          break;
        }
        case COMMAND_STEERING_RANGE_LEFT:
        {
#ifdef INVERT_STEERING
          settings.SteeringMax
#else
          settings.SteeringMin
#endif 
            = servoAngle;
          Serial.print("Set steering range left: "); Serial.println(servoAngle);  
          break;
        }
        case COMMAND_STEERING_RANGE_RIGHT:
        {
#ifdef INVERT_STEERING
          settings.SteeringMin
#else
          settings.SteeringMax
#endif 
            = servoAngle;
            
          Serial.print("Set steering range right: "); Serial.println(servoAngle);  
          break;
        }
        case COMMAND_STEERING_RANGE:
        {
          settings.SteeringMin = 0;
          settings.SteeringMax = 180;
          
          Serial.print("Reset Steering range to  0-180");  
          break;
        }

        case COMMAND_STEERING_RANGE_SAVE:
        {
          saveSettings(&settings);
          Serial.print("save steering range settings");  
          break;
        }
      }


      if(package.CommandId == COMMAND_BEEP) //BEEP
        {
#ifdef BEEP_PIN
          digitalWrite(BEEP_PIN, HIGH);
#endif
          Serial.print("beep");
        }
        else
        {
#ifdef BEEP_PIN
          digitalWrite(BEEP_PIN, LOW);
#endif
        }

   }
}

int prevThrottle = -1;

void processMotorViaHBridge(int throttle)
{
    #ifdef PWMFREQ1
      long stopTimeMs = 0;
    #else
      long stopTimeMs = 10;
    #endif

      
      if (throttle > 530 && throttle <= 1023)
      {
#ifdef TURBO_BACKWARD_PIN
        digitalWrite(motorATurboBackward, LOW);
#endif
#ifdef PWM_BACKWARD_PIN
        digitalWrite(motorABackward, LOW);
#endif
#ifdef ADD_BACKWARD_PIN
        digitalWrite(ADD_BACKWARD_PIN, LOW);
#endif
#ifdef LIGHT_BACKWARD_PIN
        digitalWrite(LIGHT_BACKWARD_PIN, LOW);
#endif

        if (prevThrottle >=0 && prevThrottle < 530)
        {
          delay (stopTimeMs); //esc dead time
        }
        //digitalWrite(motorAForward, HIGH);
        
        int pwmValue = (throttle - 530) / 493.0 * 255;
        pwmValue+=30;
        if (pwmValue > 255) pwmValue = 255;
       
#ifdef ADD_FORWARD_PIN
        digitalWrite(ADD_FORWARD_PIN, HIGH);
#endif
#ifdef LIGHT_FORWARD_PIN
        digitalWrite(LIGHT_FORWARD_PIN, HIGH);
#endif

        if (pwmValue > 230) //turbo mode
        {
#ifdef PWM_FORWARD_PIN
          digitalWrite(motorAForward, HIGH);
#endif
#ifdef TURBO_FORWARD_PIN
          digitalWrite(motorATurbo, HIGH);
#endif
        }
        else
        {
#ifdef TURBO_FORWARD_PIN
          digitalWrite(motorATurbo, LOW);
#endif
#ifdef PWM_FORWARD_PIN
          analogWrite(motorAForward, pwmValue);
#endif
        }
      }
      else if (throttle >= 0 && throttle < 500)
      {
#ifdef TURBO_FORWARD_PIN
        digitalWrite(motorATurbo, LOW);
#endif
#ifdef PWM_FORWARD_PIN
        digitalWrite(motorAForward, LOW);
#endif        
#ifdef ADD_FORWARD_PIN
        digitalWrite(ADD_FORWARD_PIN, LOW);
#endif 
#ifdef LIGHT_FORWARD_PIN
        digitalWrite(LIGHT_FORWARD_PIN, LOW);
#endif

        if (prevThrottle > 530)
        {
          delay (stopTimeMs); //esc dead time
        }

        //digitalWrite(motorABackward, HIGH);
        int pwmValue = (500-throttle) / 500.0 * 255;
        pwmValue+=30;
        if (pwmValue > 255) pwmValue = 255;

#ifdef ADD_BACKWARD_PIN
        digitalWrite(ADD_BACKWARD_PIN, HIGH);
#endif
#ifdef LIGHT_BACKWARD_PIN
        digitalWrite(LIGHT_BACKWARD_PIN, HIGH);
#endif

        if (pwmValue > 230) //turbo mode
        {
#ifdef PWM_BACKWARD_PIN
          digitalWrite(motorABackward, HIGH);
#endif
#ifdef TURBO_BACKWARD_PIN
          digitalWrite(motorATurboBackward, HIGH);
#endif
        }
        else
        {
#ifdef TURBO_BACKWARD_PIN
          digitalWrite(motorATurboBackward, LOW);
#endif
#ifdef PWM_BACKWARD_PIN        
        analogWrite(motorABackward, pwmValue);
#endif
        }

      } else
      {
        stopMotorViaHBridge();
      }
      prevThrottle = throttle;
}


void stopMotorViaHBridge()
{
#ifdef TURBO_FORWARD_PIN
        digitalWrite(motorATurbo, LOW);
#endif
#ifdef TURBO_BACKWARD_PIN
        digitalWrite(motorATurboBackward, LOW);
#endif
#ifdef PWM_FORWARD_PIN
        digitalWrite(motorAForward, LOW);
#endif
#ifdef PWM_BACKWARD_PIN
        digitalWrite(motorABackward, LOW);
#endif
#ifdef ADD_FORWARD_PIN
        digitalWrite(ADD_FORWARD_PIN, LOW);
#endif
#ifdef ADD_BACKWARD_PIN
        digitalWrite(ADD_BACKWARD_PIN, LOW);
#endif
#ifdef LIGHT_FORWARD_PIN
        digitalWrite(LIGHT_FORWARD_PIN, LOW);
#endif
#ifdef LIGHT_BACKWARD_PIN
        digitalWrite(LIGHT_BACKWARD_PIN, LOW);
#endif


    #ifdef PWMFREQ1
      long stopTimeMs = 0;
    #else
      long stopTimeMs = 10;
    #endif
    delay(stopTimeMs);

}

int calculateServoAngle(int steering, bool invert)
{
  int appAngle;
  if (invert)
    appAngle = settings.AppRight - steering; //inverse
  else
    appAngle = steering;
    
  if (appAngle < 0) 
    appAngle = 0;
        
  int servoAngle2;
  if (appAngle >= (int)settings.AppCenter)
  {
    servoAngle2 = (int)(((float)appAngle - settings.AppCenter) / (settings.AppRight - settings.AppCenter) * (settings.SteeringMax - settings.SteeringCenter) + settings.SteeringCenter);  
  }
  else
  {
    servoAngle2 = (int)(((float)appAngle - settings.AppLeft) / (settings.AppCenter - settings.AppLeft) * (settings.SteeringCenter - settings.SteeringMin) + settings.SteeringMin);  
  }  
  return servoAngle2;
}

void loopSearchForTransmitter()
{
    #ifdef PWMFREQ1
    long ms = 10;
    #else
    long ms = 100;
    #endif
    
  delay(ms);
  bool ok = radio.write(&bindData, sizeof(bindData));
  Serial.print("Beacon sent ");  
  int transmitterResponse = 0;
  if (ok){
    Serial.println("succesfully");
    mode = MODE_REGULAR;

#ifdef MOTOR_VIA_ESC
    motor.writeMicroseconds(MAX_ESC_SIGNAL);
    delay(ms);
    motor.writeMicroseconds(STOP_ESC_SIGNAL);
    delay(ms);
    motor.writeMicroseconds(MAX_ESC_SIGNAL);
    delay(ms);
    motor.writeMicroseconds(STOP_ESC_SIGNAL);
#endif

#ifdef MOTOR_VIA_HBRIDGE
    processMotorViaHBridge(800);
    delay(ms);
    stopMotorViaHBridge();
    delay(ms);
    processMotorViaHBridge(800);
    delay(ms);
    stopMotorViaHBridge();
#endif;
    
    radio.setChannel(bindData.channelNo);
    radio.openReadingPipe(1,address[1]);      //хотим слушать трубу 0
    radio.startListening();
  }
  else
    Serial.println("failed");
}

void calibrateEsc(Servo * esc)
{/*
  esc->writeMicroseconds(STOP_ESC_SIGNAL);
  delay(300);
  esc->writeMicroseconds(MAX_ESC_SIGNAL);
  delay(300);
  esc->writeMicroseconds(MIN_ESC_SIGNAL);
  delay(300);
  esc->writeMicroseconds(STOP_ESC_SIGNAL);
  */
}

void loop() 
{

#ifdef SERVO_PIN
    if (!servoInit)
    {
        servoInit = true;
        servo.write(settings.SteeringCenter);
    }
#endif

    switch (mode)
    {
      case MODE_REGULAR:
        loopRegular();
      break;  
      case MODE_SEARCH_FOR_TRANSMITTER:
        loopSearchForTransmitter();
      break;
    }
    
    
}

