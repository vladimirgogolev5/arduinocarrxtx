#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>
#include <EEPROM.h>


RF24 radio(9, 10); // "создать" модуль на пинах 9 и 10 Для Уно

byte address[][6] = {"1Node", "2Node", "3Node", "4Node", "5Node", "6Node"}; //возможные номера труб

const byte COMMAND_NULL = 0;
const byte COMMAND_NORM_STEERING = 1;
const byte COMMAND_BEEP = 2;
const byte COMMAND_STEERING_RANGE = 3;
const byte COMMAND_STEERING_RANGE_LEFT = 4;
const byte COMMAND_STEERING_RANGE_RIGHT = 5;
const byte COMMAND_STEERING_RANGE_SAVE = 6;

typedef struct PackageData
{
  unsigned int Throttle : 10;
  unsigned int Steering : 10;
  byte CommandId : 5;
  byte CommandParameter : 7;  
};

PackageData package;

const byte DATA_VERSION_KEY_VALUE = 0x12;

typedef struct SettingsData
{
  byte DataVersionKey;
  unsigned int TrottleMinimum;
  unsigned int TrottleMiddle;
  unsigned int TrottleMaximum;
  unsigned int SteeringMinimum;
  unsigned int SteeringMiddle;
  unsigned int SteeringMaximum;
};

SettingsData settings; 


const byte BIND_CHANNEL_NO = 0x5;
typedef struct BindData
{
  byte receiverId;
  byte channelNo;
  byte checkSum;
};

BindData receiverBindData;

const unsigned int INITIAL_STEERING_MIDDLE = 1023 / 2;
const unsigned int INITIAL_STEERING_MIN = 0; //left
const unsigned int INITIAL_STEERING_MAX = 1023; //right

const unsigned int INITIAL_THROTTLE_MIDDLE = 1023 / 2;
const unsigned int INITIAL_THROTTLE_MIN = 0; // back
const unsigned int INITIAL_THROTTLE_MAX = 1023; // forward

unsigned int mode;
const unsigned int MODE_REGULAR = 0;
const unsigned int MODE_SETUP_TRANSMITTER_RANGES = 1;
const unsigned int MODE_SETUP_STEERING_RANGE = 2;
const unsigned int MODE_SEARCH_FOR_RECEIVER = 3;

unsigned long prevD2time = 0;
unsigned long prevD4time = 0;
bool d2state = true;
bool d4state = false;
unsigned long lastNormButtonPressTime = 0;
unsigned long lastSetupRangesButtonPressTime = 0;
unsigned long lastSetupSteeringButtonPressTime = 0;
const unsigned long LONG_PRESS_TIME = 2000;
int a0Value;
int a1Value;
int a5Value;
unsigned long currentTime;

void setup() {
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
  
  Serial.begin(9600); //открываем порт для связи с ПК

  radio.begin(); //активировать модуль
  delay(50);
  
  radio.setChannel(BIND_CHANNEL_NO);  //выбираем канал (в котором нет шумов!)
  radio.setPayloadSize(4);     //размер пакета, в байтах
  radio.setRetries(10, 1);    //(время между попыткой достучаться, число попыток)

  radio.setDataRate (RF24_250KBPS); //скорость обмена. На выбор RF24_2MBPS, RF24_1MBPS, RF24_250KBPS должна быть одинакова на приёмнике и передатчике! при самой низкой скорости имеем самую высокую чувствительность и дальность!!
  radio.setPALevel (RF24_PA_MAX); //уровень мощности передатчика. На выбор RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX
  
  radio.setAutoAck(true);         //режим подтверждения приёма, 1 вкл 0 выкл
  radio.enableAckPayload();    //разрешить отсылку данных в ответ на входящий сигнал
//  radio.enableDynamicPayloads();
  
  radio.openReadingPipe(1,address[0]);      //хотим слушать трубу 0

  radio.powerUp(); //начать работу

  mode = MODE_SEARCH_FOR_RECEIVER; 
  radio.writeAckPayload(1,'5',1);
  radio.startListening();  //начинаем слушать эфир, режим поиска приемника
  
  loadSettings(&settings);
  if(settings.DataVersionKey != DATA_VERSION_KEY_VALUE)
  {
    settings.DataVersionKey = DATA_VERSION_KEY_VALUE;
    settings.TrottleMinimum = INITIAL_THROTTLE_MIN;
    settings.TrottleMiddle = INITIAL_THROTTLE_MIDDLE;
    settings.TrottleMaximum = INITIAL_THROTTLE_MAX;
    settings.SteeringMinimum = INITIAL_STEERING_MIN;
    settings.SteeringMiddle = INITIAL_STEERING_MIDDLE;
    settings.SteeringMaximum = INITIAL_STEERING_MAX;
  }
}

void loadSettings(SettingsData * data)
{
  int address=0;
  byte * dataPtr = (byte*)(data);
  for(int i=0; i<sizeof(*data); i++)
  {
    *dataPtr = EEPROM.read(address + i);
    dataPtr++;
  }  
}

void saveSettings(SettingsData * data)
{
  int address=0;
  byte * dataPtr = (byte*)(data);
  for(int i=0; i<sizeof(*data); i++)
  {
    EEPROM.update(address + i, *dataPtr);
    dataPtr++;
  }  
}

void regularLoop()
{
  modeLedLight(100, 900);

  Serial.print("A5:");
  Serial.print(a5Value);
  
  unsigned int steeringValue = normAnalogValue(a0Value, settings.SteeringMinimum, settings.SteeringMiddle, settings.SteeringMaximum, false);
  package.Steering = steeringValue;
    
  Serial.print(" A0:");
  Serial.print(a0Value);
  Serial.print(" ST:");
  Serial.print(steeringValue);
  
  unsigned int throttleValue = normAnalogValue(a1Value, settings.TrottleMinimum, settings.TrottleMiddle, settings.TrottleMaximum, true/*false*/);
  //throttleValue = correctThrottleAcceleration(throttleValue, 0.0, 512.0, 1023.0);
  package.Throttle = throttleValue;

  Serial.print(" A1:");
  Serial.print(a1Value);
  Serial.print(" TH:");
  Serial.print(throttleValue);
  


  if (buttonPressedForTime(a5Value, 142/*395*/, 10, &lastSetupSteeringButtonPressTime, LONG_PRESS_TIME)) //команда "нормировать диапазон руля на стороне приемника"
  {
     mode = MODE_SETUP_STEERING_RANGE;
     package.CommandId = COMMAND_STEERING_RANGE;
     Serial.print("Setup steering:");
  }

  d4state = false;
  if (buttonPressed(a5Value, 0/*58*/, 10))   
  {
    if (currentTime - lastNormButtonPressTime > 500)
    {
      package.CommandId = COMMAND_NORM_STEERING;
      d4state = true;
    }
    lastNormButtonPressTime = currentTime;
  }
  digitalWrite(4, d4state ? HIGH : LOW);  
  
  if (buttonPressed(a5Value, 13, 10))   //команда "посигналить" 
  {
    package.CommandId = COMMAND_BEEP;
    Serial.println(" beep ");
  } 

  if (buttonPressedForTime(a5Value, 416/*234*/, 10, &lastSetupRangesButtonPressTime, LONG_PRESS_TIME)) //команда "нормировать диапазон на аппаратуре"
  {
      mode = MODE_SETUP_TRANSMITTER_RANGES;
      settings.DataVersionKey = DATA_VERSION_KEY_VALUE;
      settings.TrottleMinimum = a1Value - 10;
      settings.TrottleMiddle = a1Value;
      settings.TrottleMaximum = a1Value + 10;
      settings.SteeringMinimum = a0Value - 10;
      settings.SteeringMiddle = a0Value;
      settings.SteeringMaximum = a0Value + 10;
  } 

  radio.write(&package, sizeof(package));
  Serial.print(" Sent: "); Serial.println(sizeof(package));
}

void setupTransmitterRangesLoop()
{
  Serial.print("Setup transmitter:");
  
  modeLedLight(900, 900);

  if (a1Value < settings.TrottleMinimum)
    settings.TrottleMinimum = a1Value;

  if (a1Value > settings.TrottleMaximum)
    settings.TrottleMaximum = a1Value;

  if (a0Value < settings.SteeringMinimum)
    settings.SteeringMinimum = a0Value;

  if (a0Value > settings.SteeringMaximum)
    settings.SteeringMaximum = a0Value;

  Serial.print(" Left:");
  Serial.print(settings.SteeringMinimum);
  Serial.print(", Mid:");
  Serial.print(settings.SteeringMiddle);
  Serial.print(", Right:");
  Serial.print(settings.SteeringMaximum);

  Serial.print("; Back:");
  Serial.print(settings.TrottleMinimum);
  Serial.print(", Mid:");
  Serial.print(settings.TrottleMiddle);
  Serial.print(", Fwd:");
  Serial.println(settings.TrottleMaximum);


  if (buttonPressedForTime(a5Value, 416 /*234*/, 10, &lastSetupRangesButtonPressTime, LONG_PRESS_TIME)) //команда "сохранить нормирование диапазона на аппаратуре"
  {
      mode = MODE_REGULAR;
      saveSettings(&settings);
  } 
}

void setupSteeringRangeLoop()
{
 
  modeLedLight(200, 200);

  unsigned int steeringValue = normAnalogValue(a0Value, settings.SteeringMinimum, settings.SteeringMiddle, settings.SteeringMaximum, false);
  package.Steering = steeringValue;
    
  /*Serial.print(" A0:");
  Serial.print(a0Value);
  Serial.print(" ST:");
  Serial.print(steeringValue);
  */
  unsigned int throttleValue = normAnalogValue(settings.TrottleMiddle, settings.TrottleMinimum, settings.TrottleMiddle, settings.TrottleMaximum, false);
  package.Throttle = throttleValue;
/*
  Serial.print(" A1:");
  Serial.print(a1Value);
  Serial.print(" TH:");
  Serial.println(throttleValue);
*/
  d4state = false;
  if (buttonPressedForTime(a5Value, 142 /*395*/, 10, &lastSetupRangesButtonPressTime, LONG_PRESS_TIME)) //команда "сохранить нормирование диапазона руля на приемнике"
  {
      mode = MODE_REGULAR;
      package.CommandId = COMMAND_STEERING_RANGE_SAVE;
      d4state = true;
  } 
  else if (buttonPressed(a5Value, 0/*58*/, 10))   
  {
    if (currentTime - lastNormButtonPressTime > 200)
    {
      if (a0Value < settings.SteeringMiddle)
      {
        package.CommandId = COMMAND_STEERING_RANGE_LEFT;
        Serial.print("set left");
        Serial.print(" ST:");
        Serial.println(steeringValue);
      }
      else
      {
        package.CommandId = COMMAND_STEERING_RANGE_RIGHT;
        Serial.println("set right");
        Serial.print(" ST:");
        Serial.println(steeringValue);
      }
      d4state = true;
    }
    lastNormButtonPressTime = currentTime;
  }
  digitalWrite(4, d4state ? HIGH : LOW);  
  

  radio.write(&package, sizeof(package));
}

bool buttonPressedForTime(int inputValue, int buttonValue, int buttonValueRange, unsigned long * lastButtonPressTime, unsigned long requiredPressTime)
{
  if (buttonPressed(inputValue, buttonValue, buttonValueRange)) //команда "сохранить нормирование диапазона на аппаратуре"
  {
    if (*lastButtonPressTime == 0)
    {
      *lastButtonPressTime = currentTime;
    } 
    else if (currentTime - *lastButtonPressTime > requiredPressTime 
            && currentTime - *lastButtonPressTime < requiredPressTime + 200)
    {
      *lastButtonPressTime -= 1000;
      return true;
    } 
  } 
  else
  {
    *lastButtonPressTime = 0;
  }
  return false;
}

void modeLedLight(unsigned long onTime, unsigned long offTime)
{
  if ( d2state && currentTime - prevD2time > onTime)
  {
    prevD2time = currentTime;
    d2state = false;
    digitalWrite(2, d2state ? HIGH : LOW);  
  } 
  else if ( !d2state && currentTime - prevD2time > offTime)
  {
    prevD2time = currentTime;
    d2state = true;
    digitalWrite(2, d2state ? HIGH : LOW);  
  }
}

void loop() {
  currentTime = millis();


  //A5 buttons tx1  -   -   -   -  *  *
  //               234 395 294 440 13 58
  a5Value = analogRead(A5);
  delay(2);        // delay in between reads for stability


//A5 buttons tx2 -   -    *
//              416 142   0
  //steering
  //middle = 841;
  //left = 697
  //right =  959
  a0Value = analogRead(A0);
  delay(2);        // delay in between reads for stability  

  //throttle
  //middle 315
  //forward 415
  //back 220
  a1Value = analogRead(A1);

  package.CommandId = COMMAND_NULL;
  package.CommandParameter = 0;

  switch (mode)
  {
    case MODE_REGULAR:
      regularLoop();
    break;

    case MODE_SETUP_TRANSMITTER_RANGES:
      setupTransmitterRangesLoop();
    break;

    case MODE_SETUP_STEERING_RANGE:
      setupSteeringRangeLoop();
    break;

    case MODE_SEARCH_FOR_RECEIVER:
      searchForReceiverLoop();
  }

  delay(30);
}

byte count = 0;
void searchForReceiverLoop()
{
    Serial.println("look for receiver");
    modeLedLight(900, 100);
  
    byte pipeNo;                          

    if ( radio.available(&pipeNo))
    { 
      count++;
      radio.writeAckPayload(1,count,1);
      radio.read( &receiverBindData, sizeof(receiverBindData) );
      Serial.print("receiverId ");
      Serial.print(receiverBindData.receiverId);
      Serial.print(" channelNo ");
      Serial.print(receiverBindData.channelNo);
      Serial.print(" checkSum ");
      Serial.print(receiverBindData.checkSum);
      if (receiverBindData.receiverId != 0 
        && receiverBindData.channelNo >= 0 
        && receiverBindData.channelNo <= 127
        && receiverBindData.receiverId + receiverBindData.channelNo == receiverBindData.checkSum) //если получили нормальный бинд-пакет от приемника, а не мусор
      {
        radio.stopListening();  //не слушаем радиоэфир, мы передатчик
        radio.setChannel(receiverBindData.channelNo);
        radio.openWritingPipe(address[1]);   //мы - труба 2, открываем канал для передачи данных
        mode = MODE_REGULAR;
      }
    }
}

bool buttonPressed(int inputValue, int buttonValue, int buttonValueRange)
{
  int value = inputValue - buttonValue;
  
  if (value >= -buttonValueRange && value <= buttonValueRange)
  {
    return true;
  }
  else
  {
    return false;
  }
  
}

unsigned int normAnalogValue(int inputValue, int minimum, int middle, int maximum, bool invert)
{
  unsigned int normValue;
  float tmp;
  unsigned int outputRange = 0x3FF; //полный диапазон 0...1023, т.е. 2^10
  float outputRangeHalf = 511.5; // outputRange / 2
  
  if (inputValue <= middle)
  {
     float maxrange = middle - minimum;
     if (inputValue < minimum) 
        tmp = 0;
     else
        tmp = inputValue - minimum;
     
     tmp = tmp / maxrange * outputRangeHalf;
     normValue = (unsigned int)(tmp  + 0.5); //0.5 для правильного округления
  } 
  else 
  {
     float maxrange = maximum - middle;
     if (inputValue > maximum) 
        tmp = maxrange;
     else
        tmp = inputValue - middle;
     
     tmp = tmp / maxrange * outputRangeHalf;
     tmp = tmp + outputRangeHalf; 
     
     normValue = (unsigned int)(tmp  + 0.5); //0.5 для правильного округления
     
  }

  if (normValue > outputRange) normValue = outputRange; //1023

  if (invert) 
      normValue = outputRange - normValue;
  
  return normValue;  
}

/*
float prevThrottle = 0;
unsigned long prevTime = 0;

unsigned int correctThrottleAcceleration(unsigned int throttleValue, float TrottleMinimum, float TrottleMiddle, float TrottleMaximum)
{
  unsigned long currentTime = millis();
  unsigned long dtime = currentTime - prevTime;

  float throttle = throttleValue;
  if (throttle < TrottleMinimum)
    throttle = TrottleMinimum;
  if (throttle > TrottleMaximum)
    throttle = TrottleMaximum;

  throttle = throttle - TrottleMiddle; // center middle throttle to zero

  if (throttle > 0)  //norm throttle to max 1 and min -1
  {
    throttle = throttle / (TrottleMaximum - TrottleMiddle);
  }
  else if (throttle < 0)
  {
    throttle = throttle / (TrottleMiddle - TrottleMinimum);
  }

  if (throttle > 0.02) // acceleration forward increase speed correction
  {
    float requiredDelta = throttle-prevThrottle;
    if (requiredDelta > 0)
    {
      float availableDelta = dtime * 0.002;
      if (requiredDelta > availableDelta)
        throttle = prevThrottle + availableDelta;
      if (throttle > 1)
        throttle = 1;
    }
  }
  else if (throttle < -0.02) // acceleration backward increase speed correction
  {
    //throttle *= 0.5; // make backward maximum speed slower than forward
    float requiredDelta = throttle-prevThrottle;
    if (requiredDelta < 0)
    {
      float availableDelta = dtime * -0.002; // backward acceleration is lower than forward acceleration
      if (requiredDelta < availableDelta)
        throttle = prevThrottle + availableDelta;
      if (throttle < -1)
        throttle = -1;
    }
  }
  else
    throttle = 0; //anti-tremble protection

  prevThrottle = throttle;
  prevTime  = currentTime;
  throttle = (throttle + 1.0) / 2 * 1023;
  return (unsigned int)(throttle  + 0.5); //0.5 для правильного округления 
}
*/

